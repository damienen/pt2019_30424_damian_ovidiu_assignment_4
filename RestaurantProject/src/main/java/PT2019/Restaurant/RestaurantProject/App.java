package PT2019.Restaurant.RestaurantProject;

import java.util.*;
import javax.swing.SwingUtilities;

import business.*;
import data.*;
import presentation.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SwingUtilities.invokeLater(new Runnable() {
    		public void run() {
    			new MainGraphicalUserInterface();
    		}
    	});
    }
}
