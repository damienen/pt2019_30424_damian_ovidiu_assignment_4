package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import business.*;
import data.FileWriter;

public class MainGraphicalUserInterface extends JFrame {
		Restaurant rest;
		AdministratorGraphicalUserInterface adm;
		WaiterGraphicalUserInterface wait;
	public MainGraphicalUserInterface() {
		ChefGraphicalUserInterface chef=new ChefGraphicalUserInterface();
		rest=new Restaurant(chef);
		GridBagConstraints c=new GridBagConstraints();
		JPanel panel=new JPanel(new GridBagLayout());
		this.setTitle("Ovi's restaurant");
		this.add(panel);
		JButton adminButton=new JButton("Administrator");
		JButton waiterButton=new JButton("Waiter");
		JButton chefButton=new JButton("Chef");
		c.gridx=0;c.gridy=0;
		panel.add(adminButton,c);
		c.gridx=1;
		panel.add(waiterButton, c);
		c.gridx=3;
		panel.add(chefButton,c);
		adminButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adm=new AdministratorGraphicalUserInterface(rest.getMenu());
			}
		}); 
		waiterButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wait=new WaiterGraphicalUserInterface(rest);
			}
		});
		this.addWindowListener(new  WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				FileWriter.writeMenu(rest.getMenu());
				FileWriter.writeOrders(rest.getOrders());
			}
		}
		);
		this.setPreferredSize(new Dimension(300,100));
		this.setSize(this.getPreferredSize());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);
	}
}
