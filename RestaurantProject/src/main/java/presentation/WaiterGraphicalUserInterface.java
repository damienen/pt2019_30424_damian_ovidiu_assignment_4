package presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

import business.*;
import data.FileWriter;

public class WaiterGraphicalUserInterface extends JFrame {
	JTable orderTable;
	Restaurant r;

	private DefaultTableModel updateTable() {
		String col[] = { "ID", "Date", "Table", "Items" };
		DefaultTableModel tableModel = new DefaultTableModel(col, 0);
		for (Order k : r.getOrders().keySet()) {
			String s = "";
			for (MenuItem mi : r.getOrders().get(k)) {
				if (mi instanceof BaseProduct)
					s += ((BaseProduct) mi).getName() + " ";
				else
					s += ((CompositeProduct) mi).getName() + " ";
			}
			tableModel.addRow(new Object[] { k.getOrderID(), k.getDate(), k.getTable(), s });
		}
		return tableModel;
	}

	JButton generateBill;
	JSpinner spin;
	JButton addOrder;
	JComboBox<String> selectItem;
	JButton addToOrder;
	JButton deleteFromOrder;
	ArrayList<MenuItem> io;

	public WaiterGraphicalUserInterface(final Restaurant rest) {
		r = rest;
		io = new ArrayList<MenuItem>();
		JPanel wholePanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		JPanel tablePanel = new JPanel();
		JPanel editPanel = new JPanel(new GridBagLayout());
		orderTable = new JTable(updateTable());
		tablePanel.add(orderTable);
		gbc.gridx = 0;
		gbc.gridy = 0;
		addOrder = new JButton("Add order");
		addToOrder = new JButton("Add to order");
		deleteFromOrder = new JButton("Del from order");
		generateBill = new JButton("Generate bill");
		String s[] = new String[rest.getMenu().size()];
		for (int i = 0; i < rest.getMenu().size(); i++)
			s[i] = rest.getMenu().get(i).getName();
		selectItem = new JComboBox<String>(s);
		spin = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
		gbc.gridy = 0;
		editPanel.add(new JLabel("Table"));
		gbc.gridy = 1;
		editPanel.add(spin);
		editPanel.add(addOrder, gbc);
		gbc.gridy = 5;
		editPanel.add(selectItem, gbc);
		gbc.gridy = 6;
		editPanel.add(addToOrder, gbc);
		gbc.gridy = 7;
		editPanel.add(deleteFromOrder, gbc);
		gbc.gridy = 8;
		editPanel.add(generateBill, gbc);
		gbc.gridx = 0;
		gbc.gridy = 0;

		addOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!io.isEmpty()) {
					r.getOrders().put(
							new Order(Order.maxOrdID(r.getOrders()) + 1, new Date(), (Integer) spin.getValue()), io);
				}
				orderTable.setModel(updateTable());
			}
		});
		addToOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectItem.getSelectedIndex() > -1)
					io.add(rest.getMenu().get(selectItem.getSelectedIndex()));
			}
		});
		deleteFromOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selectItem.getSelectedIndex() > -1)
					io.remove(rest.getMenu().get(selectItem.getSelectedIndex()));
			}
		});
		generateBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (orderTable.getSelectedRow() > -1) {
					rest.generateBill((Integer) orderTable.getValueAt(orderTable.getSelectedRow(), 0));
				}
			}
		});
		JScrollPane scroll = new JScrollPane(tablePanel);
		scroll.setPreferredSize(new Dimension(400, 400));
		wholePanel.add(scroll, gbc);
		gbc.gridx = 1;
		wholePanel.add(editPanel, gbc);

		this.add(wholePanel);
		this.setPreferredSize(new Dimension(800, 800));
		this.setSize(this.getPreferredSize());
		this.setLocationRelativeTo(null);
		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setVisible(true);
	}
}
