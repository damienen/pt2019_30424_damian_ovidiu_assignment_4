package presentation;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import business.*;
import business.MenuItem;

public class AdministratorGraphicalUserInterface extends JFrame {
	ArrayList<MenuItem> items;
	JTable menu;
	JTextArea nameArea;
	JTextArea priceArea;
	JButton addButton;
	JButton addRButton;
	JButton editButton;
	JButton deleteButton;
	JButton deleteRButton;
	ArrayList<MenuItem> recipe;
	private DefaultTableModel updateTable() {
		String col[] = { "Item", "Price", "Ingredients" };
		DefaultTableModel tableModel = new DefaultTableModel(col, 0);
		for (MenuItem k : items) {
			if (k instanceof BaseProduct) {
				BaseProduct bp = (BaseProduct) k;
				tableModel.addRow(new Object[] { bp.getName(), bp.computePrice(), "-" });
			} else {
				CompositeProduct cp = (CompositeProduct) k;
				tableModel.addRow(new Object[] { cp.getName(), cp.computePrice(), cp.toProductsString() });
			}
		}
		return tableModel;
	}
	public AdministratorGraphicalUserInterface(ArrayList<MenuItem> i) {
		items = i;
		recipe = new ArrayList<MenuItem>();
		JPanel wholePanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		JPanel tablePanel = new JPanel();
		JPanel editPanel = new JPanel(new GridBagLayout());
		menu = new JTable(updateTable());
		nameArea = new JTextArea("");
		nameArea.setPreferredSize(new Dimension(150, 20));
		priceArea = new JTextArea("");
		priceArea.setPreferredSize(new Dimension(150, 20));
		gbc.gridx = 0;
		gbc.gridy = 0;
		editPanel.add(new JLabel("Item:"), gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		editPanel.add(new JLabel("Price"), gbc);
		gbc.gridy = 1;
		editPanel.add(nameArea, gbc);
		gbc.gridy = 3;
		editPanel.add(priceArea, gbc);
		addButton = new JButton("Add item(menu)");
		addRButton = new JButton("Add item(recipe)");
		editButton = new JButton("Edit item(menu)");
		deleteButton = new JButton("Delete item(menu)");
		deleteRButton = new JButton("Delete item(recipe)");
		gbc.gridy = 7;
		editPanel.add(addRButton, gbc);
		gbc.gridy = 4;
		editPanel.add(addButton, gbc);
		gbc.gridy = 5;
		editPanel.add(editButton, gbc);
		gbc.gridy = 6;
		editPanel.add(deleteButton, gbc);
		gbc.gridy = 8;
		editPanel.add(deleteRButton, gbc);
		tablePanel.add(menu);
		gbc.gridx = 0;
		gbc.gridy = 0;
		JScrollPane scroll=new JScrollPane(tablePanel);
		scroll.setViewportView(tablePanel);
		wholePanel.add(scroll, gbc);
		gbc.gridx = 1;
		wholePanel.add(editPanel, gbc);
		menu.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (menu.getSelectedRow() > -1) {
					nameArea.setText(menu.getValueAt(menu.getSelectedRow(), 0).toString());
					priceArea.setText(menu.getValueAt(menu.getSelectedRow(), 1).toString());
				}}
		});
		addRButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (menu.getSelectedRow() > -1) {
					if (!recipe.contains(items.get(menu.getSelectedRow()))) {
						recipe.add(items.get(menu.getSelectedRow()));
						}
					}
			}
		});
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (recipe.isEmpty()) {
					try {
						BaseProduct bp = new BaseProduct(nameArea.getText(), Float.parseFloat(priceArea.getText()));
						if (!items.contains(bp)) {
							items.add(bp);
						}
					} catch (Exception ee) {
						ee.printStackTrace();
					}
				} else {
					try {
						CompositeProduct cp = new CompositeProduct(nameArea.getText(), recipe);
						if (!items.contains(cp))
							items.add(cp);
					} catch (Exception ee) {
						ee.printStackTrace();
					}

				}
				menu.setModel(updateTable());}
		});
		editButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (menu.getSelectedRow() > -1) {
					if (recipe.isEmpty()) {
						try {
							BaseProduct bp = new BaseProduct(nameArea.getText(), Float.parseFloat(priceArea.getText()));
							items.set(menu.getSelectedRow(), bp);
						} catch (Exception ee) {
							ee.printStackTrace();
						}
					} else {
						try {
							CompositeProduct cp = new CompositeProduct(nameArea.getText(), recipe);
							items.set(menu.getSelectedRow(), cp);
						} catch (Exception ee) {
							ee.printStackTrace();
						}
					}
				}
				menu.setModel(updateTable());}
		});
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (menu.getSelectedRow() > -1) {
					items.remove(menu.getSelectedRow());
				}
			menu.setModel(updateTable());
				}
		});
		deleteRButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (menu.getSelectedRow() > -1) {
					recipe.remove((items.get(menu.getSelectedRow())));
				}
			}
		});
		this.add(wholePanel);
		this.setPreferredSize(new Dimension(400, 800));
		this.setSize(this.getPreferredSize());
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
	}
}
