package business;

public class BaseProduct extends MenuItem{
	private String name;
	private float price;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float computePrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public BaseProduct(String name, float price) {
		super();
		this.name = name;
		this.price = price;
	}
	public BaseProduct() {
		super();
		this.name="";
		this.price=0;
	}
	@Override
	public String toString() {
		return name;
	}
}
