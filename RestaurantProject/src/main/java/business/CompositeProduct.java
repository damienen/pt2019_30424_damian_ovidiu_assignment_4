package business;

import java.util.*;

public class CompositeProduct extends MenuItem{
	private String name;
	private ArrayList<MenuItem> products;
	public CompositeProduct() {
		super();
		this.name="";
		this.products=new ArrayList<MenuItem>();
	}
	public CompositeProduct(String name, ArrayList<MenuItem> products) {
		super();
		this.name = name;
		this.products = products;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<MenuItem> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<MenuItem> products) {
		this.products = products;
	}
	public void addItem(MenuItem item) {
		products.add(item);
	}
	public void removeItem(MenuItem item) {
		products.remove(item);
	}
	public void removeItem(int index) {
		products.remove(index);
	}
	@Override
	public String toString() {
		return name +" " + computePrice()+"$";
	}
	public String toProductsString() {
		String s="";
		for(MenuItem item:products) {
			s+=item.toString()+" ";
		}
		return s;
	}
	public float computePrice() {
		float price=0;
		for(MenuItem item:products) {
			price+=item.computePrice();
		}
		return price;
	}
	
}
