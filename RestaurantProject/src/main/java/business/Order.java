package business;
import java.util.*;

import data.FileWriter;
public class Order implements java.io.Serializable{
	private int orderID;
	private Date date;
	private int table;
	public Order() {
		super();
		orderID=0;
		date=new Date();
		table=0;
	}
	public Order(int orderID, Date date, int table) {
		super();
		this.orderID = orderID;
		this.date = date;
		this.table = table;
	}
	public static int maxOrdID(HashMap<Order, ArrayList<MenuItem>> orders) {
		int ret=0;
		for(Order k:orders.keySet()) {
			if(ret<k.getOrderID())
				ret=k.getOrderID();
		}
		return ret;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getTable() {
		return table;
	}
	public void setTable(int table) {
		this.table = table;
	}
	@Override
	public int hashCode() {
		final int prime = 47;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + orderID;
		result = prime * result + table;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (orderID != other.orderID)
			return false;
		if (table != other.table)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", date=" + date + ", table=" + table + "]";
	}
	
}
