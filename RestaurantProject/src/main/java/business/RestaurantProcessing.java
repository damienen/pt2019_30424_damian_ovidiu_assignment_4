package business;

import java.util.*;
import business.*;

public interface RestaurantProcessing {
	//Administrator
	public abstract void createMenuItem(String name, float price);
	public abstract void createMenuItem(String name, ArrayList<MenuItem> items);
	public abstract void deleteMenuItem(int index);
	public abstract void editMenuItem(int index, MenuItem item);
	//Waiter
	public abstract void createOrder(int table, ArrayList<MenuItem> items);
	public abstract float computePrice(int orderID);
	public abstract void generateBill(int orderID);
}
