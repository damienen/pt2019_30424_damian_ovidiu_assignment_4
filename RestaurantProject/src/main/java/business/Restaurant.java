package business;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import data.*;
import data.FileWriter;

public class Restaurant extends Observable implements RestaurantProcessing {
	private static int numberOfTables = 10;
	private ArrayList<MenuItem> menu;
	private HashMap<Order, ArrayList<MenuItem>> orders;
	private Observer chef;

	public Restaurant(Observer chef) {
		menu = RestaurantSerializator.readMenu();
		orders = new HashMap<Order, ArrayList<MenuItem>>();
		orders = RestaurantSerializator.readOrders();
		this.chef = chef;
	}

	public static int getNumberOfTables() {
		return numberOfTables;
	}

	public static void setNumberOfTables(int numberOfTables) {
		Restaurant.numberOfTables = numberOfTables;
	}

	public ArrayList<MenuItem> getMenu() {
		return menu;
	}

	public void setMenu(ArrayList<MenuItem> menu) {
		this.menu = menu;
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		this.orders = orders;
	}

	public Observer getChef() {
		return chef;
	}

	public void setChef(Observer chef) {
		this.chef = chef;
	}

	public void createMenuItem(String name, float price) {
		BaseProduct item = new BaseProduct(name, price);
		menu.add(item);
	}
	
	public void createMenuItem(String name, ArrayList<MenuItem> items) {
		CompositeProduct item = new CompositeProduct(name, items);
		menu.add(item);
	}

	public void deleteMenuItem(int index) {
		menu.remove(index);

	}

	public void editMenuItem(int index, MenuItem item) {
		menu.set(index, item);

	}

	public void createOrder(int table, ArrayList<MenuItem> items) {
		Order od = new Order(orders.keySet().size(), new Date(), table);
		orders.put(od, items);
		notifyObserver(this, items.toString());
	}

	public float computePrice(int orderID) {
		Order od = null;
		for (Order aux : orders.keySet()) {
			if (aux.getOrderID() == orderID) {
				od = aux;
				break;
			}
		}
		float result = 0;
		if (od != null) {
			for (MenuItem item : orders.get(od)) {
				result += item.computePrice();
			}
		}
		return result;
	}

	public void generateBill(int orderID) {
		Order od = null;
		for (Order aux : orders.keySet()) {
			if (aux.getOrderID() == orderID) {
				od = aux;
				break;
			}
		}
		String text = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
		text += "Ovi's restaurant\n FIC XXXXXXXX \n Name-of-the-street street, number X\n";
		text += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
		for (MenuItem item : orders.get(od)) {
			text += item.toString() +" "+item.computePrice()+ "$\n";
		}
		text += "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
		text += "Total: " + computePrice(orderID);
		File file = new File("Bill #" + orderID + ".txt");
		try {
			Files.write(Paths.get(file.getPath()), text.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void notifyObserver(Observable observ, String s) {
		chef.update(observ, s);
	}
}
