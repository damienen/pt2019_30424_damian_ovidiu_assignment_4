package data;

import java.io.*;
import java.util.*;
import business.*;

public class FileWriter {
	public static void writeMenu(ArrayList<MenuItem> menu) {
		File file = new File("menu.ser");
		try {
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeInt(menu.size());
			for (MenuItem item : menu) {
				out.writeObject(item);
			}
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		try {
			File file = new File("orders.ser");
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeInt(orders.keySet().size());
			for (Order od : orders.keySet()) {
				out.writeObject(od);
				out.writeObject(orders.get(od));
			}
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
