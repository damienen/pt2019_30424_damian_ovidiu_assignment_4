package data;

import java.io.*;
import java.util.*;
import business.*;

public class RestaurantSerializator {
	public static ArrayList<MenuItem> readMenu() {
		ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
		try {

			File file = new File("menu.ser");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fis);
			int size=in.readInt();
			for (int i = 0; i < size; i++) {
				MenuItem item = (MenuItem) in.readObject();
				menu.add(item);
			}
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
		return menu;
	}
	public static HashMap<Order, ArrayList<MenuItem> >  readOrders() {
		HashMap<Order, ArrayList<MenuItem> > orders = new HashMap<Order, ArrayList<MenuItem> >();
		try {
			File file = new File("orders.ser");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fis);
			int oSize=in.readInt();
			for(int i=0;i<oSize;i++) {
				Order od=(Order)in.readObject();
				@SuppressWarnings("unchecked")
				ArrayList<MenuItem> oItems =(ArrayList<MenuItem>) in.readObject();
				orders.put(od, oItems);
			}
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		return orders;
	}
}
